#!/usr/bin/env bash

CRIT=${1:-15}

while read -r capacity; do
    stat="$(cat "${capacity/capacity/status}")"
    perc="$(cat "$capacity")"
    bat="$(cut --delimiter='/' -f5 <<< "$capacity")"

    if (( perc <= CRIT )) && [[ "$stat" = 'Discharging' ]]; then
        notify-send --urgency=critical 'Battery Low' "Current charge of $bat: $perc%"
    fi
done < <(find /sys/class/power_supply/*/ -type f -name capacity)
